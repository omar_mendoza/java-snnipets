package com.hibernate.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HUtil {
	
	private static SessionFactory sf = build();
	private static ServiceRegistry sr;
	// Deprecated
//	@SuppressWarnings({ "deprecation", "unused" })
//	private static SessionFactory build() {
//		return new Configuration().configure().buildSessionFactory();
//	}
	
	private static SessionFactory build () {
		Configuration conf = new Configuration();
		conf.configure();
		sr = new ServiceRegistryBuilder().applySettings(conf.getProperties()).buildServiceRegistry();
		sf = conf.buildSessionFactory(sr);
		return sf;
	}
	
	public static SessionFactory getSession() {
		return sf;
	}

	public static void close() {
		sf.close();
	}
}
