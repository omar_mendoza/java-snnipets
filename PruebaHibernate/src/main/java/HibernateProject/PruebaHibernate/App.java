package HibernateProject.PruebaHibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.config.HUtil;
import com.hibernate.models.Players;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Session session = HUtil.getSession().openSession();
        
        // En un Service de un DAO podemos poner @Transactional
       Transaction transaction = session.beginTransaction();
       
       // Añadir
//       Players player = new Players();
//       player.setName("Prueba");
//       player.setNum(100);
//       
//       Integer id = (Integer) session.save(player);
//       transaction.commit();
       
       
//       String sql = "From Player";
//       String sql = "From Players p Order by p.name";
       String sql = "From Players p Order by p.num DESC";
       Query q = session.createQuery(sql);
       List<Players> players = q.list();
       
       for(Players p : players) System.out.println(p);
       
       // Obtener por ID y borrar
       int id = (Integer) session.createQuery("select max(id) from Players ").uniqueResult();
       Players pl= (Players) session.get(Players.class, id);
       System.out.println("-----");
       System.out.println(pl.toString());
       
       session.delete(pl);
       transaction.commit();
       
       
       HUtil.close();
       
    }
}

