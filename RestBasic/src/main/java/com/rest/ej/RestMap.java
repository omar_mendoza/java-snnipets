package com.rest.ej;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@RestController
public class RestMap {

	// Método con Gson
	@RequestMapping(value="/test",
			produces={MediaType.APPLICATION_JSON_UTF8_VALUE},
			method=RequestMethod.GET)
	public ResponseEntity<String> test () {
		
		Gson gson = new Gson();
		Persona persona = new Persona("Omaró Mendoza", 32);
		
		String respuesta = gson.toJson(persona);
		
		return new ResponseEntity<String>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(value="/test2",
			produces={MediaType.APPLICATION_JSON_UTF8_VALUE},
			method=RequestMethod.GET)
	public ResponseEntity<String> test2() {
		Gson gson = new Gson();
		Persona persona = new Persona("Omar", 32);
		Persona persona2 = new Persona("Antonio", 30);
		List<Persona> personas = new ArrayList<Persona>();
		personas.add(persona);
		personas.add(persona2);
		String respuesta = gson.toJson(personas);
		return new ResponseEntity<String>(respuesta, HttpStatus.OK);
	}
	
}
