package com.example.ws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WSController {
	
	@RequestMapping(value="/test", produces=MediaType.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
	public ResponseEntity<Persona> test() {
		Persona persona = new Persona("Omar", 32);
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}
	
	@RequestMapping(value="/test2", produces=MediaType.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)
	public ResponseEntity<Collection<Persona>> test2() {
		Persona persona = new Persona("Omar", 32);
		Persona persona2 = new Persona("Omaro", 32);
		List<Persona> personas = new ArrayList<Persona>();
		personas.add(persona);
		personas.add(persona2);
		return new ResponseEntity<Collection<Persona>>(personas, HttpStatus.OK);
	}

}
