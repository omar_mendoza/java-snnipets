package org.ejemplo;

import java.util.List;

import org.ejemplo.dao.DAO;
import org.ejemplo.model.Persona;
import org.ejemplo.model.Persona2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {

		ApplicationContext ac = new ClassPathXmlApplicationContext("spring_conf.xml");
		
////		Persona p = ac.getBean(Persona.class);
//		Persona p = (Persona) ac.getBean("persona");
//		
////		p.imprimir();
//		
//		System.out.println(p);
//		
		
		DAO dao = (DAO) ac.getBean("dao");
		
		List<Persona> personas = dao.list();
		
		Persona persona = new Persona();
		persona.setNombre("Nuevo3");
		persona.setEdad("25");
		
		
		int ocurrencia = 0;
		for(Persona p : personas) {
			if(p.getNombre().equals(persona.getNombre())) {
				ocurrencia = 1;
				break;
			}
				
		}
		
		if(ocurrencia == 0) dao.save(persona);
		
		for(Persona p : personas) System.out.println(p);
		
		
		System.out.println("------");
		personas = dao.findByName("Nu");
		for(Persona p : personas) System.out.println(p);
		
//		System.out.println(dao.findById(2));
		
		
		((ClassPathXmlApplicationContext)ac).close();
		

	}

}
