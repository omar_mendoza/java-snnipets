package org.ejemplo.model;

import org.springframework.beans.factory.annotation.Autowired;

public class Persona2 {
	
	private String nombre;
	private int edad;
	
	private Direccion dir;
	
	@Autowired // Puede ser también en el setter
	private Direccion2 dir2;
	
	public Persona2() {}
	
	public Persona2(String nombre, int edad) {
		this.nombre = nombre;
		this.edad = edad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Direccion getDir() {
		return dir;
	}

	public void setDir(Direccion direccion) {
		this.dir = direccion;
	}

	public Direccion2 getDir2() {
		return dir2;
	}

	public void setDir2(Direccion2 dir2) {
		this.dir2 = dir2;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", dir=" + dir
				+ ", dir2=" + dir2 + "]";
	}

	public void imprimir() {
		System.out.println("Persona");
	}

}
