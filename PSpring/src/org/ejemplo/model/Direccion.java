package org.ejemplo.model;

public class Direccion {
	
	private String direccion;

	public Direccion() {}
	
	public Direccion(String direccion) {
		super();
		this.direccion = direccion;
	}

	private void setDireccion(String dir) {
		this.direccion = dir;
	}

	@Override
	public String toString() {
		return "Direccion [direccion=" + direccion + "]";
	}
	
	
}
