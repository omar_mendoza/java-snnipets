package org.ejemplo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component // Clase candidata a ser inyectada
public class Direccion2 {
	private String direccion;

	public Direccion2() {
	}

	public String getDireccion() {
		return direccion;
	}

	@Autowired
	public void setDireccion(@Value("Dirección2 de Omaro") String direccion) {
		this.direccion = direccion;
	}

	@Override
	public String toString() {
		return "Direccion2 [direccion=" + direccion + "]";
	}

}
