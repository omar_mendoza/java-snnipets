package org.ejemplo.dao;

import java.util.List;

import javax.sql.DataSource;

import org.ejemplo.model.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("dao")
public class DAOImpl implements DAO {

	private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbc = new NamedParameterJdbcTemplate(dataSource);
	}
	
	public List<Persona> list() {
		
		String sql = "Select * From persona";
		List<Persona> personas = jdbc.query(sql, new BeanPropertyRowMapper<Persona>(Persona.class));
		
		return personas;
		
	}

	public boolean save(Persona p) {
		
//		MapSqlParameterSource paramMap = new MapSqlParameterSource();
//		paramMap.addValue("nombre", p.getNombre());
//		paramMap.addValue("edad", p.getEdad());
		
		BeanPropertySqlParameterSource paramMap = new BeanPropertySqlParameterSource(p);

		return jdbc
				.update("insert into persona (nombre, edad) values(:nombre, :edad)", paramMap) == 1;
	}

	@Override
	public Persona findById(int id) {
		return jdbc.queryForObject("select * from persona where id=:id", new MapSqlParameterSource("id",id), Persona.class);
//		return (Persona) jdbc
//				.query("select * from persona where id=:id", 
//						new MapSqlParameterSource("id", id),
//						new BeanPropertyRowMapper<Persona>(Persona.class));
	}

	@Override
	public List<Persona> findByName(String nombre) {
		return jdbc
				.query("select * from persona where nombre like :nombre", 
						new MapSqlParameterSource("nombre", "%"+nombre+"%"), 
						new BeanPropertyRowMapper<Persona>(Persona.class));
	}

	@Override
	public boolean delete(int id) {
		return jdbc.
				update("delete from persona where id=:id", new MapSqlParameterSource("id",id))==1;
	}

	@Override
	@Transactional
	public void saveAll(List<Persona> personas) {
		SqlParameterSource [] batchArgs = SqlParameterSourceUtils.createBatch(personas.toArray());
		String sql = "insert into persona (nombre, edad) values(:nombre, :edad)";
		jdbc.batchUpdate(sql, batchArgs);
	}
	
	

}
