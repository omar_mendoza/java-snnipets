package org.ejemplo.dao;

import java.util.List;

import org.ejemplo.model.Persona;

public interface DAO {
	
	public List<Persona> list(); 
	public boolean save(Persona p);
	public Persona findById(int id);
	public List<Persona> findByName(String nombre);
	public boolean delete(int id);
	
	public void saveAll(List<Persona> personas);

}
