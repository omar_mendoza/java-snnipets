<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	
	<script src="<c:url value="/resources/lib/jquery.js" />" type="text/javascript"></script>	
	<script type="text/javascript">
		$(document).on('ready', function(){
			
			var _url = "http://localhost:8080/ej/test"
			var _url2 = "http://localhost:8080/ej/test2"
			
			// Varias maneras de llamar un ws
			
			// Iterar sobre un arreglo de json
 			$.get(_url2, function(data) {
				console.log(".GET");
				$.each(data, function(i, item) {
					console.log(item.nombre + " " + item.edad);
				})
 			});
			
			$.getJSON(_url, function(data) {
				console.log("DATA --> "  + data.nombre);
			});
			
			$.ajax({
				url: _url
			}).then(function(data) {
				console.log("DATA --> " + data);
				$('#mostrar').append(data);
			});
			$.ajax({
		        type: 'GET',
		        url:  _url,
		        dataType: 'json',
		        async: true,
		        success: function(result) {
		            console.log("DATA--> " + result);
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            console.log(jqXHR.status + ' ' + jqXHR.responseText);
		        }
		   });	
		});
	</script>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>

<div id="datos">
	<p>Mostrar</p>
	<p id="mostrar"></p>
</div>
</body>
</html>
